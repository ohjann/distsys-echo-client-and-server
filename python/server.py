import socket
import threading
import queue
import time
from optparse import OptionParser

# Setup program flags and descriptions
parser = OptionParser()
parser.add_option("-d", "--debug", action="store_true", dest="debug", help="put server in debug mode where it is intentionally slowed down")
parser.add_option("-p","--port", action="store", type="int", dest="pnumber", metavar="PORT NUMBER", help="set custom port number, defaults to 8080")
parser.add_option("-i","--IP", action="store", type="string", dest="hnumber", metavar="HOST IP", help="set custom host IP, defaults to empty string")
(options, args) = parser.parse_args()

if(not options.pnumber and (len(args) >0)):
    parser.error("incorrect args\nType -h or --help for help.")

if(options.hnumber):
    host = options.hnumber
else:
    host = ''
if(options.pnumber):
    port = options.pnumber
else:
    port = 8000
serverstatus = ":)"
queue = queue.Queue()

class ThreadClients (threading.Thread):
    """Handles connected clients concurrently"""
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        global serverstatus
        connection = self.queue.get()
        while (serverstatus == ":)"):
            try:
                data = connection[0].recv(1024).decode()
            except: break
            if "HELO" in data:
                print ("*Sending message to ", connection[1][0])
                passedText = data.split("HELO",1)[1].split("\n",1)[0].strip()
                textToSend = ("HELO "+passedText+"\nIP:"+host+"\nPort:"+str(port)+"\nStudentID:11424478").encode()
                connection[0].sendall(textToSend)

            elif "KILL_SERVICE" in data:
                print ("*Kill request recieved from ", connection[1][0])
                serverstatus = ":("
                connection[0].sendall(("Server terminating\n").encode())
                break

            else:
                print ("*Malformed request from", connection[1][0])
                connection[0].sendall(("ERROR: Malformed request\n").encode())

            if (options.debug): time.sleep(2)
            if (data == ''): break

        print("Number of connections left in queue ", self.queue.qsize())
        connection[0].close()
        self.queue.task_done()

def startThread(queue):
    thread = ThreadClients(queue)
    thread.setDaemon(True)
    thread.start()

def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    s.bind((host, port))
    s.listen(5)

    for i in range(5):
        startThread(queue)

    while True:
        print("\t\t"+serverstatus)
        if(serverstatus == ":("): break
        conn, addr = s.accept()
        # don't allow any more than 6 clients to connect at once
        print('Connected by ', addr, queue.qsize())
        queue.put((conn,addr)) # tuple in form of (socket connection, (client address))
        queue.join()
        startThread(queue)

    print("Exiting");
    s.close()

main()
