import socket
from optparse import OptionParser


# Setup program flags and descriptions
parser = OptionParser()

parser.add_option("-p","--port", action="store", type="int", dest="pnumber", metavar="PORT NUMBER", help="set custom port number")
parser.add_option("-f","--file", action="store", type="string", dest="filename", metavar="FILENAME", help="use custom file")
parser.add_option("-m","--message", action="store", type="string", dest="message", metavar="MESSAGE", help="change default message to custom message")
parser.add_option("-k","--kill", action="store_true", dest="kill", help="send kill message to server")
(options, args) = parser.parse_args()

if(not options.pnumber and  not options.filename and not options.message and (len(args) >0)):
    parser.error("incorrect args\nType -h or --help for help.")

if(options.pnumber):
    port = options.pnumber
else:
    port = 8000

if(options.filename):
    path = options.filename
else:
    path = "/echo.php"


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = 'localhost'

try:
    s.connect((host,port))


##    if(options.kill):
    ##    message = path+"?message='KILL_SERVICE\n'"
    ##elif(options.message):
    ##    message = path+"?message='"+options.message+"\n'"
    ##else:
    ##    message = path+"?message='HELO ello\n'"

    message = path+"?message='HELO ello\n'"
    s.sendall(message.encode('utf-8'))
    response = s.recv(1024)
    response = response #+ s.recv(1024)
    print (response)

    #s.close() #close connection once a response has be given

    #s.connect((host,port))
    message = path+"?message='O lo\n'"
    s.sendall(message.encode('utf-8'))
    response = s.recv(1024)
    response = response# + s.recv(1024)
    print (response)
    #s.close() #close connection once a response has be given

    #s.connect((host,port))
    if(options.kill):
        message = path+"?message='KILL_SERVICE\n'"
        s.sendall(message.encode('utf-8'))
        response = s.recv(1024)
        response = response #+ s.recv(1024)
        print (response)
    s.close() #close connection once a response has be given


except:
    print("soz no")
