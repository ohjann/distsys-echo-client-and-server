import subprocess
import threading

# Test if server can handle a large amount of clients attempting to connect at the same time

class callClient(threading.Thread):
    def run(self):
        result = subprocess.check_output(["python", "client.py"])
        print("%s Result:\n%s" %(self.getName(), result))


for i in range(50):
    t = callClient()
    t.start()

result = subprocess.check_output(["python", "client.py","-k"])
print("EXITING Result:\n%s" % (result))
